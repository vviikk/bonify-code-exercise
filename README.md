# Coding-exercise

- [CSS in JS (advantages, drawbacks, and how styled-components is hopefully the answer)](packages/opinion-css-in-js)
- [Normalizing Javscript Object](packages/normalize-application-data) - tl;dr; use ES2015 Proxies :0
- [Babel plugin to prefix console outputs](packages/babel-plugin-transform-console-prefixer)
