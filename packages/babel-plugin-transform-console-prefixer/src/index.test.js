import * as babel from 'babel-core'
import path from 'path'
import plugin from '.'
import pluginTester from 'babel-plugin-tester' // eslint-disable-line import/order

// Credit to https://github.com/jamiebuilds/babel-handbook/blob/master/translations/en/plugin-handbook.md#toc-unit-testing

pluginTester({
  plugin,
  pluginName: 'babel-plugin-transform-console-prefixer',
  tests: [
  ],
  fixtures: path.join(__dirname, '__fixtures__'), // Please see assertions in the __fixtures__ directory
  snapshot: true,
  babelOptions: {
    babelrc: true,
  },
})

// Seems like 'snapshot: true' above doesn't work, so
// a small snapshot test below
const prefix = 'Bonify rocks'
const example = `
const foo = 1;
if (foo) console.log(foo);`

const transformedExample = `
const foo = 1;
if (foo) console.log("${prefix}", foo);`

it('prefixes console.log and matches snapshot', () => {
  const { code } = babel.transform(example, { plugins: [[plugin, { prefix }]] })
  expect(code).toEqual(transformedExample)
  expect(code).toMatchSnapshot()
})

// Want to write AST tests, but lack of time.
