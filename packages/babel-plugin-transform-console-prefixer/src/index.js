const { defaults } = require('../package.json')

// Mutating the console.log output is one of my favorite questions for
// candidates who express a knowledge in javascript

module.exports = ({ types } = {}) => ({
  visitor: {
    CallExpression(path, state) {
      const config = { ...defaults, ...state.opts } // overridable using .babelrc

      if (
        !config.prefix // check if prefix is defined
        || !path.node.callee.object // ensure valid node
        || !path.node.callee.property // ensure valid node
        || path.node.callee.object.name !== config.logger // check if the logger matches the config
        || config.loggerFunction.indexOf(path.node.callee.property.name) < 0
        // ^^ check if it's log function
      ) return

      // prefix the log function with a custom string
      path.node.arguments.unshift(types.stringLiteral(config.prefix))
    },
  },
})
