/* eslint-disable import/prefer-default-export, no-nested-ternary */

// uses ES6 Proxies
export const normalize = (data) => {
  const handler = {
    // using the undefined way to check keys for
    // perfomance reasons http://jsben.ch/WqlIl
    get: (target, key) => (target[key] !== undefined && target[key].value
        !== undefined) // I check if the key 'value' exists
      ? (
        Array.isArray(target[key].value) // I check if value is an array
          ? target[key].value.map(item => normalize(item)) // and normalize each item in array
          : target[key].value) // or return the value of the item
      : target[key], // if value isn't a nested object, I simple return the existing value
  }

  // return a proxied object
  return new Proxy(data, handler)
}
